# This is my README
#Author: Charles Gathuru
#Student no: 260457189
The program shell.c requires a header file def.h to function which contains all
the global vairables, included libraries and function protoypes used by the
program.

The program was complied with gcc-4.8 during testing. The header file and the
source code should be kept in the same folder. To execute complie and execute.
No command line agruments are needed during execution on the a.out file that is produced to run the shell program.

While running the shell if there are no processes running the <crtl><z> will
not add the process to the background because it will cause the shell to quit,
so nothing will happen. Consequently, only exit will cause the shell to exit.

Trying to run a command from history that does not exist will print a command
not found error and will not add it to the history because the command is not
a user entered command, but a history command that is not valid.  

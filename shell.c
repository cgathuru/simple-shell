/* 
 * File:   shell.c
 * Author: Charles Gathuru
 * Student no: 260457189
 *
 * Created on January 21, 2014, 10:53 PM
 */

#include "def.h"

/*
 * 
 */
int main(void)
{
    
    main_pid= getpid();
    shell_pid = getppid();
    
    char inputBuffer[MAX_LINE]; /* buffer to hold the command entered */
    int background;             /* equals 1 if a command is followed by '&' */
    char *args[MAX_LINE/+1];    /* command line (of 80) has max of 40 arguments */ 
    int histIndex = 0;
    
    if(signal(SIGINT, sig_handler) == SIG_ERR)
        printf("Cant catch signal <ctrl><c>\n");
    if(signal(SIGTSTP, sig_handler) == SIG_ERR)
        printf("Cant catch signal <ctrl><z>\n");

    cmdNo=0;
    //TODO move everything below to new method the call it after erroneous cmd
    //TODO echo all history commands to the screen
    while (1)                   /* Program terminates normally inside setup */
    {
        updateStatus();
        background =0;
        /* reset the bypass environment variables */
        bypassExec = false;
        bypassHist = false;
        printf(" COMMAND->\n");
        setup(inputBuffer,args,&background, false);    /* get next command */
        checkBuiltCommands(inputBuffer,args,background,histIndex);
        /* If we need to execute a command without adding it to the history */
        if(!bypassHist)
        {
            addCommandToHistory(args,histIndex);
            cmdNo++;
        }
        pid_t pid;
        pid = fork();
        if(pid != 0)      /* Parent Process */
        {
            if(background == 0)
            {
                int status;
                pid_t waitP = waitpid(pid,&status,WUNTRACED); /* Wait for the child process to terminate */
                if(status == 0)
                    hist_error[histIndex]=1;
                else                    /* The command executed had an error */
                {
                    hist_error[histIndex]= 0;
                }
            }
            else
            {
                putProcessBackground(pid);
            }
            /* Do not increment the history index if history was bypassed*/
            if(!bypassHist)
            {
                /* If all the indices of the array are full the do not
                 * increment the history index */
                if(histIndex<MAX_HIST)
                    histIndex++;
            }
            
        }
        else            /* Child process */
        {
            if(!bypassExec)
                execvp(args[0], args);            
        }
        /*the steps are:
         (1) fork a child process using fork()
         (2) the child process will invoke execvp()
         (3) if background ==0, the parent will wait,
                otherwise returns to the setup() function. */
    }
}


/**
* setup() reads in the next command line, separating it into distinct tokens
* using whitespace as delimiters. setup() sets the args parameter as a
* null-terminated string.
*/
int setup(char inputBuffer[], char *args[],int *background, bool skip)
{
   int length, /* # of characters in the command line */
           i,  /*loop index for accessing inputBuffer array */
           start, /* index where beginning of next command parameter is */
           ct; /* index of where to place the next parameter into args[] */
   
   
   ct=0;
   
   if(!skip){
       /* read what the user enters on the command line */
        length = read(STDIN_FILENO, inputBuffer, MAX_LINE);

        start = -1;
        if (length == 0)
           exit(0);        /* ^d was entered, end of user command stream */
        if (length < 0)
        {
           perror("error reading the command" +'\n');
           return ERROR;       /* terminate with error code of -1 */
        }
   }
   else{
       start = -1;
       length = string_length(inputBuffer);
   }

   /* examine every character in the inputBuffer */
   for (i=0;i<length;i++)
   {
       switch (inputBuffer[i])
       {
           case ' ':
               
           case '\t':          /* argument separators */
               if(start != -1)
               {
                   args[ct] = &inputBuffer[start];      /* set up pointer */
                   ct++;
               }
               
               inputBuffer[i] = '\0'; /* add a null char; make a C string */
               start = -1;
               break;
               
           case '\n':   /* should be the final char examined */
               if(start != -1)
               {
                  args[ct] = &inputBuffer[start];
                  ct++;
               }
               inputBuffer[i] = '\0';
               args[ct] = NULL; /* no more arguments to this command */
               break;
               
           default :    /* some other character */
               if (start == -1)
                   start = i;
               if (inputBuffer[i] == '&')
               {
                   *background = 1;
                   inputBuffer[i] = '\0';
                   if( start == i )
                       start = -1;
               }
               
       } //end switch
   }//end for
   args[ct] = NULL; /* just in case the input line was >80 */
   return SUCCESS;
}//end setup

/* checks if the command entered by the user is a built in command and
 * processes the command accordingly. If it is not a built in command
 * the nothing is done */
void checkBuiltCommands(char inputBuffer[], char *args[], int background, int index)
{
    if (strcmp(inputBuffer,"exit") == 0)
    {
        killAll();
        exit(0);
    }
    else if (strcmp(inputBuffer,"history") == 0)
    {
        int i;
        printf("History \n");
        for(i=0;i<MAX_HIST;i++)
        {
            /* If the history at the index is empty do no print it */
            if(strlen(cmd_hist[i]) != 0)
                printf("%i  %s \n",i,cmd_hist[i]);
        }
        bypassExec = true;
    }
    else if (strcmp(inputBuffer,"r") == 0)
    {
        if(args[1] != NULL)     /* argument passed in for history */
        {
            int histIndex = getHistIndex(args[1]);
            if(histIndex == -1)  /* command not found in history */
            {
                perror("Command not in history");
                bypassExec = true; 
                bypassHist = true; //do not add bad command to history
            }
            else        /* command  found in history */
            {
                validityCheck(args,background,histIndex);
            }
        }
        else            /* No argument passed in for history */
        {
            validityCheck(args,background,index-1);
        }
    }
    else if (strcmp(inputBuffer,"cd") == 0)
    {
        int ret;
        if (args[1])
            ret = chdir(args[1]);
        bypassExec= true;
    }
    else if (strcmp(inputBuffer,"jobs") == 0)
    {
        printJobs();
    }
    else if (strcmp(inputBuffer,"fg") == 0)
    {
        putProcessForeground(args[1]);
    }
    else
    {
        
    }

}

/* Adds the command the user entered into the command history */
void addCommandToHistory(char *args[], int histIndex)
{
    char tmp[MAX_LINE] = "";
    int i=0;
    
    while( args[i] != NULL )
    {
        strcat(tmp, args[i]);
        strcat(tmp, " ");
        i++;
    }
    
    if( cmdNo > MAX_HIST)
    {
        for(i=MAX_HIST-1; i>0; i--)
        {
            strncpy(cmd_hist[i-1],cmd_hist[i],sizeof(char)*MAX_LINE);
        }
    }
    strncpy(cmd_hist[histIndex],tmp, sizeof(char)*MAX_LINE);
    
}

/* Returns the index of first occurrence of a command in history.
 * If the command is not found -1 is returned.
 */
int getHistIndex(char string[])
{
    int i;
    for(i=MAX_HIST-1;i>-1;i--)
    {
        if(cmd_hist[i][0] == string[0])
            return i;
    }
    return -1;
}

/* Checks if the command in history that the use wants to execute is valid.
 * If it not valid the it is judged to be an erroneous command */
void validityCheck(char *args[], int background, int index)
{
    if(!isCommandValid(index))    /* check if command is erroneous */
    {
        printf("Error: Erroneous command \n");
        bypassExec = true;
        bypassHist = true; //Do not add erroneous command to history
    }
    else
    {
        setup(cmd_hist[index],args, &background, true);
        printf("%s",args[0]);
    }
}

/* Returns true if the command stored in history is a valid command */
bool isCommandValid(int index)
{
    if(hist_error[index] == 0)
    {
        return false;
    }
    return true;
}

/* the signal handler function*/
void sig_handler(int signo)
{
    /* If the signal is <ctrl><c> do not do anything */
    if(signo == SIGINT); 
    else if(signo == SIGTSTP) //quit signal
    {
        pid_t process = getpid();
        /* Do not put the main process running the shell into the background
         * or it will cause the program to quit */
        if(process != main_pid)
            putProcessBackground(getpid());
    }
}

/* Inserts a job into the job list */
t_job* insertJob(pid_t pid)
{
    t_job *newJob = malloc(sizeof(t_job));
    
    newJob->pid = pid;
    newJob->next = NULL;
    
    if(jobsList == NULL)        /* If there are no jobs in the job list */
    {
        numActiveJobs++;
        return newJob;
    }
    else
    {
        t_job *indexNode = jobsList;
        while(indexNode->next != NULL)
        {
            indexNode = indexNode->next;
        }
        newJob->id = indexNode->id +1;
        indexNode->next = newJob;
        numActiveJobs++;
        return jobsList;
        
    }
}

/* This will remove a job given a process ID*/
t_job* removeJob(pid_t pid)
{
    t_job* job = getJob(pid, BY_PROCESS_ID);
    if(jobsList == NULL)
        return NULL;
    
    t_job* currentJob = jobsList->next;
    t_job* previousJob = jobsList;
    
    while(currentJob != NULL)
    {
        if(currentJob->pid == job->pid)
        {
            numActiveJobs--;
            previousJob->next = currentJob->next;
        }
        /* If the previous job is not the root job */
        if(previousJob->pid != NULL)
        {
           previousJob = currentJob;
           currentJob = currentJob->next;
        }
        
    }
    return jobsList;
}

/* Searches for a job by jobID, processID or jobStatus*/
t_job* getJob(int searchTerm, int searchType)
{
    t_job* job= jobsList;
    switch(searchType)
    {
        case BY_JOB_ID:
            while(job != NULL)
            {
                if(job->pid == searchTerm)
                {
                    return job;
                }
                else
                {
                   job = job->next; 
                }
            }
            break;
        case BY_PROCESS_ID:
            while(job != NULL)
            {
                if(job->id == searchTerm)
                {
                    return job;
                }
                else
                {
                    job = job->next;
                }
            }
            break;
        default:
            return NULL;
            break;
    }
    return NULL;
 
}

/* Checks for any exited child processes and removes them
 * from the jobs list */
void updateStatus()
{
    int i;
    int status;
    t_job* job = jobsList;
    
    while(job != NULL)
    {
        /* If background process has completed */
        if(waitpid(job->pid,&status,WNOHANG) == job->pid)
            {
                sleep(1);
                removeBackgroundProcess(job->pid);
            }
        job = job->next;
    }
}

/*  puts a job in the background */
void putProcessBackground(pid_t pid)
{
    insertJob(pid);
    raise(SIGQUIT);
}

/* Puts a job in the foreground, removing it from the background */
void putProcessForeground(pid_t pid)
{
    removeBackgroundProcess(pid);
    kill(pid,SIGCONT);
}

/* Removes a background process */
void removeBackgroundProcess(pid_t pid)
{
    removeJob(pid);
}

/* Kills all processes running the background*/
void killAll()
{
    int ret;
    int i;
    
    t_job* job = jobsList;
    /* Do nothing if no jobs in job list*/
    if(job == NULL)
        return;
    while(job != NULL)
    {
        ret = kill(job->pid, SIGKILL);
        job = job->next;
    }
}

/* Prints all the jobs in the job list */
void printJobs()
{
    t_job* job = jobsList;
    if(job == NULL)
    {
        printf("No jobs \n");
    }
    else
    {
        while (job != NULL)
        {
            printf("[%i]        %i\n",job->id,job->pid);
            job = job->next;
        }
    }
}

/* Returns an integer representing the length of a string */
int string_length(char str[])
{
   int i;
   for(i=0; i<80; i++)
   {
	 if(str[i]=='\0')
	 {
	    return(i);
	 }
   }
}
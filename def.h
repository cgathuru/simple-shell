/* 
 * File:   definitions.h
 * Author: Charles Gathuru
 * Student no: 260457189
 *
 * Created on January 22, 2014, 7:53 AM
 */

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <stdbool.h>
#include <sys/wait.h>


#define MAX_LINE 80 /* 80 chars per line, per command, should be enough. */
#define MAX_HIST 10 /* 10 most recent commands */
#define BUFFER_SIZE 50
#define SUCCESS 1
#define FAILURE 0
#define ERROR -1

#define BY_JOB_ID 0
#define BY_PROCESS_ID 1


static char cmd_hist[MAX_HIST][MAX_LINE];  /* 10 most recent commands */
static int hist_error[MAX_HIST];
static int hist_index = 0;
static int cmdNo;       /* index in the command history */
static bool bypassExec;
static bool bypassHist;
static int numActiveJobs = 0;


typedef struct job {
        int id;
        pid_t pid;
        struct job *next;
} t_job;

static t_job* jobsList = NULL;


static pid_t shell_pid;
static pid_t main_pid;


int setup(char inputBuffer[], char *args[],int *background, bool skip);
void checkBuiltCommands(char inputBuffer[], char *args[], int background, int index);
int getHistIndex(char string[]);
void validityCheck(char *args[], int background, int index);
bool isCommandValid(int index);
void addCommandToHistory(char *args[], int histIndex);
int string_length(char str[]);
void sig_handler(int signo);
t_job* insertJob(pid_t pid);
t_job* removeJob(pid_t pid);
t_job* getJob(int searchTerm, int searchType);
void printJobs();
void updateStatus();
void putProcessBackground(pid_t pid);
void putProcessForeground(pid_t pid);
void removeBackgroundProcess(pid_t pid);
void killAll();
